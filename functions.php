<?php
/*
 * This is the child theme for Astra theme, generated with Generate Child Theme plugin by catchthemes.
 *
 * (Please see https://developer.wordpress.org/themes/advanced-topics/child-themes/#how-to-create-a-child-theme)
 */

add_action( 'wp_enqueue_scripts', 'codemax_enqueue_styles' );
function codemax_enqueue_styles() 
{
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', [ 'parent-style' ] );
}

add_action( 'wp_enqueue_scripts', 'codemax_enqueue_scripts' );
function codemax_enqueue_scripts() 
{
    wp_enqueue_script( 'button-tabs', get_stylesheet_directory_uri().'/ui/js/button-tabs.js', [ 'jquery' ], NULL, TRUE );
}


//** *Enable upload for webp image files.*/
function webp_upload_mimes( $existing_mimes ) 
{
    $existing_mimes[ 'webp' ] = 'image/webp';
    return $existing_mimes;
}
add_filter( 'mime_types', 'webp_upload_mimes' );
