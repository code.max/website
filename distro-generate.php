<?php
require_once './vendor/autoload.php';

use codemax\distro\Distro;

$distro = new Distro( __DIR__.'/distro-config.json' );

$distro->source()->path( __DIR__ );

$distro->generate();