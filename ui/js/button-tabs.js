var codemax = 
{
	widget: 
	{
		tab:
		{
			selectedHash: "",
			select: function( hash ) 
			{
				if( codemax.widget.tab.selectedHash !== hash )
				{
					if( codemax.widget.tab.selectedHash !== "" )
					{
						jQuery( '.cm-button-tab a[href="'+codemax.widget.tab.selectedHash+'"]' ).removeClass( 'cm-button-tab-selected' );
						jQuery( codemax.widget.tab.selectedHash ).addClass( 'cm-hide' );
						codemax.widget.tab.selectedHash = ""; 
					}
	
					codemax.widget.tab.selectedHash = hash;
					jQuery( '.cm-button-tab a[href="'+hash+'"]' ).addClass( 'cm-button-tab-selected' );
					
					let section = jQuery( hash );
					section.removeClass( 'cm-hide' );
					
					jQuery( 'html, body' ).animate(
						{ scrollTop: section.offset().top },
						800,
						'linear',
						function() 
						{
							if( hash !== window.location.hash )
							{
								window.location.hash = hash;	
							}
						}
					);
				}
			}
		}
	}
};

jQuery( 
	function() 
	{
		jQuery( '.cm-button-tab a' ).on(
				'click',
				function( event ) 
				{
					if ( this.hash !== "" ) 
					{
						event.preventDefault();
						codemax.widget.tab.select( this.hash );
					}
				}
		);
		if( window.location.hash ) 
		{
			codemax.widget.tab.select( window.location.hash );
		} 			
	}
);
