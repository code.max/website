window.codemax = 
{
	fixed:
	{
		hashes: 
		{
			"#sitios-personales": "cm-event-scrolltop-2500",
			"#institucionales": "cm-event-scrolltop-2000",
			"#tiendas-online": "cm-event-scrolltop-1000",
			"#educacion-virtual": "cm-event-scrolltop-1500",
			"#desarrollo-a-medida": "cm-event-scrolltop-3000",
		}
	}
};

jQuery( 
		function() 
		{
			for( let hash in window.codemax.fixed.hashes )
			{
				jQuery( 'a[href="'+hash+'"]' ).addClass( window.codemax.fixed.hashes[ hash ] );
			}
		}
);
